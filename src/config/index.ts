`use strict`;

import dotenv from "dotenv/config";
import { Config as MySQLConfig } from "../libs/types/mysql";
import { GeneralConfs } from "../libs/types/general";
import Obj from "../libs/utils/obj";
import assert from "assert";

const {
	runCallBackThroughObject
} : {
	runCallBackThroughObject: Function
} = Obj;

export default class Config{
	mysql : MySQLConfig;
	general : GeneralConfs;
	
	constructor() {
		try {
			const {
				MYSQL_DB: database,
				MYSQL_HOST: host,
				MYSQL_USER: user,
				GENERAL_PORT: port,
				GENERAL_ENV: environment
			} = process.env;

			this.mysql = {
				database,
				host,
				user
			};
			
			runCallBackThroughObject( this.mysql, (obj: any, property: string) => assert( `The property ${ property } was not found in the .env file` ) )

			this.general = {
				port: parseInt( port ? port : "3000" ),
				environment
			}

			runCallBackThroughObject( this.general, (obj: any, property: string) => assert( `The property ${ property } was not found in the .env file` ) )
		} catch ( error ) {
			throw error;
		}

	};

	reLoadEnvFile(){
		try {
			const {
				MYSQL_DB: database,
				MYSQL_HOST: host,
				MYSQL_USER: user,
				GENERAL_PORT: port,
				GENERAL_ENV: environment
			} = process.env;

			this.mysql = {
				database,
				host,
				user
			};
			
			runCallBackThroughObject( this.mysql, (obj: any, property: string) => assert( `The property ${ property } was not found in the .env file` ) )

			this.general = {
				port: parseInt( port ? port : "3000" ),
				environment
			}

			runCallBackThroughObject( this.general, (obj: any, property: string) => assert( `The property ${ property } was not found in the .env file` ) )
		} catch ( error ) {
			throw error;
		};
	};
};