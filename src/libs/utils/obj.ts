`use strict`

const runCallBackThroughObject = ( obj: any, callback: Function ) => {
	try {
		for( let property in obj ) {
			callback( obj, property );
		};
	} catch ( error ) {
		throw error;
	}
};

export default {
	runCallBackThroughObject
}