`use strict`;

export type GeneralConfs = {
	port: number | undefined,
	environment: string | undefined,
}