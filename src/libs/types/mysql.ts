`use strict`;

export type Config = {
	host: string | undefined,
	user: string | undefined,
	database: string | undefined,
}