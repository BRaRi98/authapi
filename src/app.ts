`use strict`;

// Imports
import express, { Application } from "express";
import morgan from "morgan";
import Config from "./config";

export class App{
	private app: Application;
	private config: Config;
	
	constructor(private port?: number | number){
		// Initializations
		this.app = express();
		this.config = new Config();
	};

	setup(){
		const { general } = this.config;
		const { port } = general;
		// Settings
		this.app.set( 'PORT', this.port || port );

		// Middlewares
		this.app.use( express.json() );
		this.app.use( morgan('dev') );

		// Endpoints
	};

	listen(){
		// Starting the server
		this.app.listen( this.app.get('PORT') );

		console.log( `Server on port ${ this.app.get( 'PORT' ) }` );
	};

}