`use strict`;

// Imports
import { App } from "./app";

function main(){
	const app = new App();
	app.setup();
	app.listen();
}

main();