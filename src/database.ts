`use strict`;

// import { createPool, Pool } from "mysql2/promise";
import { createPool, Pool } from "mysql";

const connect = async (): Promise<Pool> => {
	const connection = await createPool( {
		"host": "localhost",
		"user": "root",
		"database": "typescript_practice"
	} );
	return connection;
};
